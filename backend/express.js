const express = require('express')
const mongoose = require('mongoose')

// Connexion à la base de données
mongoose.connect('mongodb://localhost:27017/ma_base_de_donnees', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Connexion réussie à la base de données'))
  .catch(err => console.error('Erreur de connexion à la base de données:', err))

// Définition du modèle de données
const Schema = mongoose.Schema
const maCollectionSchema = new Schema({
  champ1: String,
  champ2: Number,
  champ3: Boolean
})
const MaCollection = mongoose.model('MaCollection', maCollectionSchema)

// Configuration de l'application Express
const app = express()
app.use(express.json())

// Route de test
app.get('/', (req, res) => {
  res.send('Backend en Express qui communique avec une base de données')
})

// Route de création d'un nouvel objet dans la base de données
app.post('/maCollection', (req, res) => {
  const nouvelObjet = new MaCollection(req.body)
  nouvelObjet.save()
    .then(() => res.send('Objet créé avec succès'))
    .catch(err => res.status(500).send(err))
})

// Route de récupération de tous les objets de la collection
app.get('/maCollection', (req, res) => {
  MaCollection.find()
    .then(objets => res.send(objets))
    .catch(err => res.status(500).send(err))
})

// Démarrage du serveur
const port = 3000
app.listen(port, () => {
  console.log(`Serveur démarré sur le port ${port}`)
})
